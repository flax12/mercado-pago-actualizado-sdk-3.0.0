<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Mercado Pago</title>

    <style>
     #form-checkout {
        display: flex;
        flex-direction: column;
        max-width: 600px;
      }

      .container-mercadopago {
        height: 25px;
        display: inline-block;
        border: 1px solid rgb(118, 118, 118);
        border-radius: 2px;
        padding: 1px 2px;
      }
    </style>

  </head>
  <body>
    <div class="container pb-5">
      <div class="accordion accordion-flush shadow p-3 mb-5 bg-body-tertiary rounded" id="accordionFlushExample">
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
              Tarjeta
            </button>
          </h2>
          <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
              <p>
                <img src="https://www.mercadopago.com/org-img/MP3/API/logos/visa.gif" class="img-fluid" alt="...">
                <img src="https://www.mercadopago.com/org-img/MP3/API/logos/amex.gif" class="img-fluid" alt="...">
                <img src="https://www.mercadopago.com/org-img/MP3/API/logos/master.gif" class="img-fluid" alt="...">
                <img src="https://www.mercadopago.com/org-img/MP3/API/logos/diners.gif" class="img-fluid" alt="...">

              </p>
              <form id="form-checkout">
                <div id="form-checkout__cardNumber" class="container-mercadopago"></div>
                <div id="form-checkout__expirationDate" class="container-mercadopago"></div>
                <div id="form-checkout__securityCode" class="container-mercadopago"></div>
                <input type="text" id="form-checkout__cardholderName" />
                <select id="form-checkout__issuer"></select>
                <select id="form-checkout__installments"></select>
                <select id="form-checkout__identificationType"></select>
                <input type="text" id="form-checkout__identificationNumber" />
                <input type="email" id="form-checkout__cardholderEmail" />

                <button type="submit" class="btn-primary" id="form-checkout__submit">Pagar</button>
                <!--<progress value="0" class="progress-bar">Cargando...</progress>-->

                <div class="progress mt-2" role="progressbar" aria-label="Basic example" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                  <div class="progress-bar" style="width: 0%"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
              Pago en efectivo 
            </button>
          </h2>
          <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
              <p>
              (BCP, BBVA Continental u otros) <img src="https://www.mercadopago.com/org-img/MP3/API/logos/pagoefectivo_atm.gif" class="img-fluid" alt="...">
              </p>
              <hr />
              <form id="form-checkout-pagoefectivo"  method="post">
                <div class="row">
                  <div class="col-5">
                    <label class="form-label" for="email">E-mail</label>
                    <input class="form-control" id="form-checkout__email" name="email" type="text" required>
                  </div>
                  <div class="col-3">
                    <label class="form-label" for="identificationType">Tipo de documento</label>
                    <select class="form-control" id="identificationType" name="identificationType" required></select>
                  </div>
                  <div class="col-4">
                    <label class="form-label" for="identificationNumber">Número del documento</label>
                    <input class="form-control" id="identificationNumber" name="identificationNumber" type="text" required>
                  </div>
                </div>

                <div>
                  <div>
                    <input type="hidden" name="transaction_amount" id="transaction_amount" value="10000">
                    <input type="hidden" name="description" id="description" value="Nome do Produto">
                    <input type="hidden" name="payment_method_id" id="payment_method_id"  value="pagoefectivo_atm">
                    <br>
                    <button class="btn-primary" type="button" onclick="sendpayment()">Pagar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
            Banca por internet o móvil
            </button>
          </h2>
          <div id="flush-collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
              <hr />
              <form id="form-checkout-bancaInternet" >
                <div class="row">
                  <div class="col-5">
                    <label class="form-label" for="form-checkout__emailBanca">E-mail</label>
                    <input class="form-control" id="form-checkout__emailBanca" name="email" type="text" required>
                  </div>
                  <div class="col-3">
                    <label class="form-label" for="identificationTypeBanca">Tipo de documento</label>
                    <select class="form-control" id="identificationTypeBanca" name="identificationType" required></select>
                  </div>
                  <div class="col-4">
                    <label class="form-label" for="identificationNumbeBancar">Número del documento</label>
                    <input class="form-control" id="identificationNumberBanca" name="identificationNumber" type="text" required>
                  </div>
                </div>

                <div>
                  <div>
                    <input type="hidden" name="transaction_amount" id="transaction_amountBanca" value="10000">
                    <input type="hidden" name="description" id="descriptionBanca" value="Nome do Produto">
                    <input type="hidden" name="payment_method_id" id="payment_method_idBanca"  value="pagoefectivo_atm">
                    <br>
                    <button class="btn-primary" type="button" onclick="sendpaymentBanca()">Pagar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://sdk.mercadopago.com/js/v2"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
      const mp = new MercadoPago("TEST-9768a80e-4df2-4390-b732-736a8ff7ef27");
      console.log(mp)
      
    const cardForm = mp.cardForm({
      amount: "100", //puede venir de una vista anterior
      iframe: true,
      form: {
        id: "form-checkout",
        cardNumber: {
          id: "form-checkout__cardNumber",
          placeholder: "Numero de tarjeta",
        },
        expirationDate: {
          id: "form-checkout__expirationDate",
          placeholder: "MM/YY",
        },
        securityCode: {
          id: "form-checkout__securityCode",
          placeholder: "Código de seguridad",
        },
        cardholderName: {
          id: "form-checkout__cardholderName",
          placeholder: "Titular de la tarjeta",
        },
        issuer: {
          id: "form-checkout__issuer",
          placeholder: "Banco emisor",
        },
        installments: {
          id: "form-checkout__installments",
          placeholder: "Cuotas",
        },        
        identificationType: {
          id: "form-checkout__identificationType",
          placeholder: "Tipo de documento",
        },
        identificationNumber: {
          id: "form-checkout__identificationNumber",
          placeholder: "Número del documento",
        },
        cardholderEmail: {
          id: "form-checkout__cardholderEmail",
          placeholder: "E-mail",
        },
      },
      callbacks: {
        onFormMounted: error => {
          if (error) return console.warn("Form Mounted handling error: ", error);
          console.log("Form mounted");
        },
        onSubmit: event => {
          event.preventDefault();

          const {
            paymentMethodId: payment_method_id,
            issuerId: issuer_id,
            cardholderEmail: email,
            amount,
            token,
            installments,
            identificationNumber,
            identificationType,
          } = cardForm.getCardFormData();

          let options = {
            url : "./process.php",
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            data: JSON.stringify({
              token,
              issuer_id,
              payment_method_id,
              transaction_amount: Number(amount),
              installments: Number(installments),
              description: "Descripción del producto",
              payer: {
                email,
                identification: {
                  type: identificationType,
                  number: identificationNumber,
                },
              },
            })
          }

          let result = axios(options).then((resultPayment) => {
            console.log("resultpayment ", resultPayment)
          }).catch((error)=> {
            console.log("error ", error)
          })

          /*fetch("./process.php", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              token,
              issuer_id,
              payment_method_id,
              transaction_amount: Number(amount),
              installments: Number(installments),
              description: "Descripción del producto",
              payer: {
                email,
                identification: {
                  type: identificationType,
                  number: identificationNumber,
                },
              },
            }),
          }).then((resultPayment) => {
            console.log("resultPayment ", resultPayment)
          })*/
        },
        onFetching: (resource) => {
          console.log("Fetching resource: ", resource);

          // Animate progress bar
         /* const progressBar = document.querySelector(".progress-bar");
          progressBar.removeAttribute("value");

          return () => {
            progressBar.setAttribute("value", "0");
          };*/

          let progressBar = document.querySelector(".progress-bar");
          for (let index = 1; index <= 100; index++) {
            progressBar.style.width = index+'%';
          }

          return () => {
            progressBar.style.width = "0%"
          };
        }
      },
    });


    </script>

    <script>

      function createSelectOptions(elem, options, labelsAndKeys = { label : "name", value : "id"}){
        const {label, value} = labelsAndKeys;

        elem.options.length = 0;
        const tempOptions = document.createDocumentFragment();

        options.forEach( option => {
          const optValue = option[value];
          const optLabel = option[label];

          const opt = document.createElement('option');
          opt.value = optValue;
          opt.textContent = optLabel;

          tempOptions.appendChild(opt);
        });

        elem.appendChild(tempOptions);
      }

      // Get Identification Types
      (async function getIdentificationTypes () {
        try {
            const identificationTypes = await mp.getIdentificationTypes();
            const docTypeElement = document.getElementById('identificationType');
            const docTypeElementFormBanca = document.getElementById('identificationTypeBanca');

            createSelectOptions(docTypeElement, identificationTypes)
            createSelectOptions(docTypeElementFormBanca, identificationTypes)

        }catch(e) {
            return console.error('Error getting identificationTypes: ', e);
        }
      })()


      //enviar formuilario de pago en efectivo:

      function sendpayment(){
        let options = {
          url : "./process_payment_cash.php",
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          data: JSON.stringify({
            payment_method_id: document.getElementById('payment_method_id').value,
            transaction_amount: Number(document.getElementById('transaction_amount').value),
            description: document.getElementById('description').value,
            payer: {
              email : document.getElementById('form-checkout__email').value,
              identification: {
                type: document.getElementById('identificationType').value,
                number: document.getElementById('identificationNumber').value,
              },
            },
          })
        }

        let result = resquest(options)
      }

      function sendpaymentBanca(){
        let options = {
          url : "./process_payment_cash.php",
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          data: JSON.stringify({
            payment_method_id: document.getElementById('payment_method_idBanca').value,
            transaction_amount: Number(document.getElementById('transaction_amountBanca').value),
            description: document.getElementById('descriptionBanca').value,
            payer: {
              email : document.getElementById('form-checkout__emailBanca').value,
              identification: {
                type: document.getElementById('identificationTypeBanca').value,
                number: document.getElementById('identificationNumberBanca').value,
              },
            },
          })
        }

        let result = resquest(options)
      
      }


      function resquest(options){
        let result = axios(options).then((resultPayment) => {
            console.log("resultpayment ", resultPayment)
            return resultPayment
        }).catch((error)=> {
            console.log("error ", error)
            return error
        })
      }
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>