<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Mercado Pago</title>

    <style>

      
    </style>

  </head>
  <body>
    <div class="container">
      <div id="paymentBrick_container"></div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://sdk.mercadopago.com/js/v2"></script>

    <script>
      const mp = new MercadoPago('TEST-9768a80e-4df2-4390-b732-736a8ff7ef27', {
        locale: 'es-AR'
      });
      const bricksBuilder = mp.bricks();
        const renderPaymentBrick = async (bricksBuilder) => {
          const settings = {
            initialization: {
              /*
                "amount" es el monto total a pagar por todos los medios de pago con excepción de la Cuenta de Mercado Pago y Cuotas sin tarjeta de crédito, las cuales tienen su valor de procesamiento determinado en el backend a través del "preferenceId"
              */
              amount: 10000,
              payer: {
                firstName: "",
                lastName: "",
                email: ""
              }
            },
            customization: {
              visual: {
                style: {
                  theme: 'dark' // | 'dark' | 'bootstrap' | 'flat',
                },
              },
              paymentMethods: {
                creditCard: "all",
                debitCard: "all",
										ticket: "all",
										bankTransfer: "all",
										atm: "all",
										onboarding_credits: "all",
										wallet_purchase: "all",
                maxInstallments: 1
              },
            },
            callbacks: {
              onReady: () => {
                /*
                Callback llamado cuando el Brick está listo.
                Aquí puede ocultar cargamentos de su sitio, por ejemplo.
                */
              },
              onSubmit: ({ selectedPaymentMethod, formData }) => {
                // callback llamado al hacer clic en el botón de envío de datos
                return new Promise((resolve, reject) => {
                  formData.description = "aqui puedes agregar la descripcion del producto"
                
                  fetch("./process.php", {
                    method: "POST",
                    headers: {
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify(formData),
                  })
                    .then((response) => response.json())
                    .then((response) => {
                      console.log("response ", response)
                      // recibir el resultado del pago
                      resolve();
                    })
                    .catch((error) => {
                      // manejar la respuesta de error al intentar crear el pago
                      reject();
                    });
                });
              },
              onError: (error) => {
                // callback llamado para todos los casos de error de Brick
                console.error(error);
              },
            },
          };
          window.paymentBrickController = await bricksBuilder.create(
            "payment",
            "paymentBrick_container",
            settings
          );
        };
        renderPaymentBrick(bricksBuilder);
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>