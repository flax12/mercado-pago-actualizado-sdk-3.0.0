<?php

  ini_set ( 'display_errors', 1 );
  error_reporting ( E_ALL );

  require_once 'vendor/autoload.php';
  use MercadoPago\MercadoPagoConfig;
  use MercadoPago\Client\PaymentMethod\PaymentMethodClient;

  MercadoPagoConfig::setAccessToken("TEST-8186041043694491-072319-931f56043f8282cf49be27042e2be04f-530866383");

  $client = new PaymentMethodClient();
  $payment_method = $client->list();

  echo json_encode($payment_method->getResponse()->getContent());

?>