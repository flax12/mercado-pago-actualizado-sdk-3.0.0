<?php 

ini_set ( 'display_errors', 1 );
error_reporting ( E_ALL );

require_once 'vendor/autoload.php';

use MercadoPago\Client\Payment\PaymentClient;
use MercadoPago\Exceptions\MPApiException;
use MercadoPago\MercadoPagoConfig;
use MercadoPago\Client\Common\RequestOptions;

try {

  MercadoPagoConfig::setAccessToken("TEST-8186041043694491-072319-931f56043f8282cf49be27042e2be04f-530866383");

  $client = new PaymentClient();
  $request_options = new RequestOptions();
  $request_options->setCustomHeaders(["X-Idempotency-Key: <SOME_UNIQUE_VALUE>"]);

  $json = file_get_contents('php://input');
  $data = json_decode($json);

  //print_r($data);

  $payment = $client->create([
    "transaction_amount" => (float)   $data->transaction_amount,
    "description" => $data->description,
    "payment_method_id" => $data->payment_method_id,
    "payer" => [
      "email" => $data->payer->email,
      "identification" => [
        "type" => $data->payer->identification->type,
        "number" => $data->payer->identification->number
      ]
    ]
  ]);
  //var_dump($payment->getResponse()->getContent());
  echo json_encode($payment);
  
} catch (MPApiException $e) {
  echo "Status code: " . $e->getApiResponse()->getStatusCode() . "\n";
  echo "Mesagge: " . $e->getApiResponse()->getContent()["message"] . "\n";
  echo "Error: " . $e->getApiResponse()->getContent()["error"] . "\n";
}


?>